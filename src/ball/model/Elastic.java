package ball.model;

import ball.Ball;

public class Elastic extends Behavious {
    public static final int GROWTH_RATE = 2;

    static final int GROW = 1;
    static final int SHRINK = -1;

    private int growthDirection;

    Elastic(int growthDirection) {
        this.growthDirection = growthDirection;
    }

    @Override
    public void update(BallImpl ball) {
        this.ball = ball;
        growthDirection = reverseGrowthDirectionIfNecessary();
        this.ball.radius = next();
    }

    /***********************************************************************************
     *
     * Do not change Elastic ALGORITHM below.
     *
     ***********************************************************************************/

    private int reverseGrowthDirectionIfNecessary() {
        if (growingTooBig() || shrinkingTooSmall()) {
            return switchDirection();
        }

        return this.growthDirection;
    }

    private boolean shrinkingTooSmall() {
        return ball.radius <= 0 && shrinking();
    }

    private boolean growingTooBig() {
        return ball.radius >= Ball.DEFAULT_RADIUS && growing();
    }

    private int switchDirection() {
        return growing() ? SHRINK : GROW;
    }

    private int next() {
        return ball.radius + (GROWTH_RATE * growthDirection);
    }

    private boolean shrinking() {
        return growthDirection == SHRINK;
    }

    private boolean growing() {
        return growthDirection == GROW;
    }
}
