package ball.model;

import ball.Ball;

public class BallFactory {

    public static Ball[] all() {
        return new Ball[]{
                bouncingBall(75, 50, Bouncing.DOWN),
                elasticBall(250, 100, Ball.DEFAULT_RADIUS, Elastic.SHRINK),
                bouncingAndElasticBall(450, 100, Ball.DEFAULT_RADIUS, new int[]{new Integer(Bouncing.DOWN), new Integer(Elastic.SHRINK)})
                // bouncingElasticBall() --> Let's make a new ball!
        };
    }

    public static Ball bouncingBall(int centerX, int centerY, int direction) {
        return new BallImpl(centerX, centerY, new Behavious[]{new Bouncing(direction)});
    }

    public static Ball elasticBall(int centerX, int centerY, int radius, int direction) {
        return new BallImpl(centerX, centerY, new Behavious[]{new Elastic(direction)}, radius);
    }
    public static Ball bouncingAndElasticBall(int centerX, int centerY, int radius, int[] direction) {
        return new BallImpl(centerX, centerY, new Behavious[]{new Bouncing(direction[0]), new Elastic(direction[1])}, radius);
    }
}
