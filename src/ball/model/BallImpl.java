package ball.model;

import ball.Ball;

import java.awt.*;

public  class BallImpl implements Ball {
    protected int x;
    protected int y;
    protected int radius;
    protected Behavious[] behaviouses;
    protected BallImpl(int x, int y, Behavious[] behaviouses, int radius) {
        this.x = x;
        this.y = y;
        this.radius = radius;
        this.behaviouses = behaviouses;
    }

    protected BallImpl(int x, int y, Behavious[] behaviouses) {
        this(x, y, behaviouses, DEFAULT_RADIUS);
    }

    // DO NOT CHANGE
    @Override
    public int radius() {
        return radius;
    }

    // DO NOT CHANGE
    @Override
    public Point center() {
        return new Point(x, y);
    }

    @Override
    public  void update(){
        for (Behavious behavious:behaviouses){
            behavious.update(this);
        }
    };
}
